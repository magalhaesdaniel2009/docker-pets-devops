# Docker Pets

## Configuração no Google Cloud Plataform

Além de uma conta do GCP, você precisará de quatro coisas para para provisionar sua infraestrutura:

- **Um projeto GCP**: O GCP organiza recursos em projetos. [Crie um agora](https://console.cloud.google.com/projectcreate) no console do GCP. Você precisará do * ID do projeto * mais tarde. Você pode ver uma lista de seus projetos no [cloud-resource-manager](https://console.cloud.google.com/cloud-resource-manager).
- **Google Compute Engine**: Você vai precisar habilitar o Google Compute Engine para o seu projeto. Faça isso agora [no console](https://console.developers.google.com/apis/library/compute.googleapis.com). Certifique-se de que o projeto que você criou esteja selecionado e clique no botão "Ativar".
- **Uma chave de conta de serviço do GCP**: O Terraform acessará sua conta do GCP usando uma chave de conta de serviço. [Crie um agora](https://console.cloud.google.com/apis/credentials/serviceaccountkey) no console. Ao criar a chave, use as seguintes configurações:
  - Selecione o projeto que você criou na etapa anterior.
  - Em "Conta de serviço", selecione "Criar Conta de Serviço".
  - Dê a ele o nome que quiser..
  - Para o papel, escolha "Project -> Editor".
  - Após salvar, adicione uma nova chave do tipo JSON, e salve o arquivo em sua máquina.
- **Adicionar uma chave SSH nos metadados do Compute Engine**: Você vai precisar gerar uma chave SSH e adicionar aos metadados do Compute Engine, para acesso remoto as VMs. Faça isso [no console](https://console.cloud.google.com/compute/metadata/sshKeys).

[Saiba mais](https://cloud.google.com/iam/docs/creating-managing-service-account-keys) sobre as chaves da conta de serviço na documentação do Google.

> Fonte: [Terraform - Setting up GCP](https://learn.hashicorp.com/tutorials/terraform/google-cloud-platform-build?in=terraform/gcp-get-started#setting-up-gcp)

## Variáveis CI/CD

Além dos passos acima, você também vai precisar definir algumas variáveis nas configurações de CI/CD do GitLab. São elas:

| Variável                | Descrição                                            |
| ----------------------- | ---------------------------------------------------- |
| TF_VAR_credentials_file | Conteúdo do JSON da chave de conta de serviço do GCP |
| TF_VAR_project          | ID do projeto GCP                                    |
| TF_VAR_ssh_user         | Usuário de acesso a VM                               |
| TF_VAR_ssh_private_key  | Chave SSH privada para acesso a VM                   |

## Estágios da Pipeline

- **build**: Será realizado o build da imagem docker, e publica-la no registry do próprio projeto no GitLab.
- **terraform:check**: Será realizada a validação dos arquivos de configuração do Terraform.
- **terraform:plan**: Serão gerados e salvos os planos de execução do Terraform.
- **terraform:apply**: Aqui os planos de execução serão aplicados para provisionar sua infraestrutura
- **deploy**: Será realizado o deploy do docker-compose na VM provisionada pelo Terraform
- **terraform:destroy**: Limpa os recursos que foram criados quando o ambiente for "parado". Obs: Depende de uma ação manual.

